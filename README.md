# Worldofmusic

Worldofmusic is an application that can be used to retrieve and aggregate all kinds of music information
             available from different sources on the Internet.

## Getting Started

You can run the application by maven with

```
java -jar worldofmusic.jar
```

### Prerequisites

When you work on a Windows OS you should be able to run the application without any additional action.

When you work on a linux based OS make sure you have installed java on your running system.

```
sudo apt-get install java
```

### Usage

Once you have your application running by following the steps in #Getting Started section you can use the application. 
Here are to possible ways to use it.

* via Postman or something equally
```
create a POST request
http://localhost:8080/api/oldBigAlbums
Make sure the Content-Type is set to 'plain/text'.
In the body of that request you can define a specific outputUrl by writing it there in plain text.
```

* via curl 
```
curl -H "Content-Type: text/plain" http://localhost:8080/api/oldBigAlbums -d 'blub.xml'
>>> {"outputUrl":"./blub.xml"}
```

Like in the postman request the additional body data is optional. You can simply call the application without.
```
curl -X POST http://localhost:8080/api/oldBigAlbums
>>> {"outputUrl":"./output.xml"}
```

## Running the tests

```
mvn test
```

## Implementation decisions
* MVC
The decision to organize and implement the application with the MVC concept is based on the requirements at this application.
There will be a possible frontend (not in this implementation) which uses the application. 
The frontend-backend communication is often realized via Rest. 
This leads us to the Spring Boot RestController implementation (`MusicDataController`) which handles the incoming Rest-calls.
The entities are represented by models.
This concept is "clean" and makes it easy to expand.

* DOM to get the data from xml
I used a DocumentBuilder and manually parsing to get the "incoming" data into my models. Why?
Of course it would be possible to use a library like `jackson` to map the data directly into the models.
It is far less code and convenient once you have configured the library call as you desired. 
Parsing the data by hand results in more code but it offers more options.
Handling the mapping by yourself will let you have the full control about it.
So you could add additional calculation on the data. 

* Why the `index.html`?
To indicate that there would be a frontend to call the Worldofmusic API. 

## Upcoming features

* Use another online service instead of the MusicMoz service
As you can see in the code in services it is intended, that each service which is used has its own service class. 
The models of the incoming data are service specific as well.
This pattern strictly separate different services from each other so there is no mixing in their logic.
To bind to the new service create models to mirror the data content and create a new service to map the xml data with the new models.
At this point we just have to change the function calls in the `MusicDataController` to the new functions. 

* New feature "list all Releases with at least 2 Compact Discs"
Simply add an additional api endpoint method to the `MusicDataController`. 
It could be implemented like this: 
```
@PostMapping("/multiDiscs")
    public ResponseEntity<?> multiDiscs(...) {....}
```
There call the function to get the music data represented by their models and make calculations with them 
to filter for the required data.
I would recommend a regular expression to compare the titles of the entries on equality until a term of (CD | Disc | Vol) appears.
This should be hint enough to be sure that there is a multiDisc album.
At least insert the filtered data into a newly created model to return and write it into a new xml file.

## Built With

* [Spring Boot](https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/html/) - The framework used
* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Florian Meklenburg** 

